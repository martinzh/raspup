//
//  Grafica.h
//

#ifndef Grafica_h
#define Grafica_h

#include <stdio.h>
#include "ofMain.h"
#include "Params.h"


#define NVIS 9
#define NCONT 16
#define NSOUND 24

class Grafica {

public:

	vector<float> volY;
	int activaContenido;
	int numCont;

	bool pixels;
	bool pixelShape;
	bool shaderFXVis;
	bool shaderFXCam;
	int  pixelAudio;

	void update(int& activaContenido);

	//=========== FBO SCENES FUNCTIONS ===========///
	void mascarilla(ofFbo& fbo, ofVec2f centro, int numSegMasc, int radioMasc, int ancho, int alto);
	
	void helpF(ofFbo& fbo);
	void plot(float* sound, float scale, float offset);
	

	void master(ofFbo &fbo, float *sound, Params &params);
	
	void config(   float* sound, int* controller, Params &params);
	void moon(     float* sound, int* controller, Params &params);
	void circs(    float* sound, int* controller, Params &params);
	void spiral(   float* sound, int* controller, Params &params);
	void rays(     float* sound, int* controller, Params &params);
	void spectrum( float* sound, int* controller, Params &params);
	void confetti( float* sound, int* controller, Params &params);
	void tunnel(   float* sound, int* controller, Params &params);
	void tentacles(float* sound, int* controller, Params &params);
	void triditest(float* sound, int* controller, Params &params);

	void min_cam(ofFbo& fbo, ofTexture &videoTexture, float* sound, int* controller, Params &params);

	void min_shader_cam(ofFbo& fbo, ofTexture &videoTexture, ofShader shader, float* sound, int* controller, Params &params);

	Grafica();

	private:

	};

	#endif /* Grafica_h */
