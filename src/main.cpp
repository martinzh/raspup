#include "ofMain.h"
#include "ofApp.h"

int main() {

	// ofGLESWindowSettings settings;
	// settings.glesVersion=2;

	// ofSetupOpenGL(1024 + 32, 768, OF_WINDOW);
	ofSetupOpenGL(1024 + 32, 768, OF_FULLSCREEN);
	ofRunApp(new ofApp());
}
