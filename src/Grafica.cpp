//
//  Grafica.cpp
//

#include "Grafica.h"

Grafica::Grafica(){
	activaContenido = 0;
	numCont         = 3;
	pixels          = false;
	pixelShape      = false;
	pixelAudio      = 0;
	shaderFXVis     = false;
	shaderFXCam     = false;
}

//--------------------------------------------------------------

void Grafica::config(float *sound, int *controller, Params &params)
{
	float ang = TWO_PI / 24.0;
	float r_circ;
	ofColor local_col;

	ofPushMatrix();

	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3] + params.posMap[1]);
	ofScale(params.global_scale);

	for (int i = 0; i < 24; i++)
	{
		r_circ = ofMap(sound[i], 0.0, params.sens_sound, 10, params.screen_dims[3], true);

		local_col.setHsb(params.test_hue, 255, 255, 200);
		ofSetColor(local_col);
		ofFill();

		ofBeginShape();
		ofVertex(0, 0);
		ofVertex(r_circ * cos(i * ang - HALF_PI)      , r_circ * sin(i * ang - HALF_PI));
		ofVertex(r_circ * cos((i + 1) * ang - HALF_PI), r_circ * sin((i + 1) * ang - HALF_PI));
		ofEndShape(true);

		local_col.setHsb(params.test_hue, 255, 255, 250);
		ofSetColor(local_col);
		ofNoFill();

		ofBeginShape();
		ofVertex(0, 0);
		ofVertex(r_circ * cos(i * ang - HALF_PI)      , r_circ * sin(i * ang - HALF_PI));
		ofVertex(r_circ * cos((i + 1) * ang - HALF_PI), r_circ * sin((i + 1) * ang - HALF_PI));
		ofEndShape(true);
	}

	ofPopMatrix();

}

//--------------------------------------------------------------
void Grafica::helpF(ofFbo& fbo) {
	fbo.begin();
	ofPushStyle();
	ofClear(0, 80);
	ofSetColor(255);
	ofDrawBitmapString("A Y U D A", fbo.getWidth()*.2, 100);
	ofPopStyle();
	fbo.end();
}

//--------------------------------------------------------------
void Grafica::master(ofFbo& fbo, float* sound, Params &params){
	
	fbo.begin();
		
		ofClear(0,0,0,0);
		ofBackground(0, 255*params.opaGral);

		if(params.set_mask){
			ofPushMatrix();
			
			ofTranslate(params.screen_dims[1] + params.posMap[0], 
				params.screen_dims[3]+params.posMap[1]);

			ofScale(params.global_scale);

			ofSetColor(0);
			// ofFill();
			ofBeginShape();
			ofVertex(params.screen_dims[1], 0);

			for (int i = 0; i < 24; i++) {
				ofVertex(
					cos(-HALF_PI + params.mask_angle*i)*params.screen_dims[3], 
					sin(-HALF_PI + params.mask_angle*i)*params.screen_dims[3]);
			}

			ofVertex(
				cos(-HALF_PI)*params.screen_dims[3],
				sin(-HALF_PI)*params.screen_dims[3]);

			ofVertex(0, -2.0*params.screen_dims[2]);
			ofVertex(2.0*params.screen_dims[0], -2.0*params.screen_dims[2]);
			ofVertex(2.0*params.screen_dims[0], 2.0*params.screen_dims[2]);
			ofVertex(-2.0*params.screen_dims[0], 2.0*params.screen_dims[2]);
			ofVertex(-2.0*params.screen_dims[0], -2.0*params.screen_dims[2]);
			ofVertex(0, -2.0*params.screen_dims[2]);
			ofVertex(0, -params.screen_dims[3]);
			ofEndShape();

			ofPopMatrix();
		}

		if(params.mapeaMidi){

			ofPushMatrix();

			ofTranslate(params.screen_dims[1] + params.posMap[0],
						params.screen_dims[3] + params.posMap[1]);

			ofSetColor(255, 0, 0, 255);

			// DRAW LINES MARKING CENTER OF THE SCREEN
			ofDrawLine(0, -params.screen_dims[3], 0, params.screen_dims[3]);
			ofDrawLine(-params.screen_dims[1], 0, params.screen_dims[1], 0);

			// DRAW CIRCLE MARKING THE SPAN OF VISUALS
			ofPushMatrix();

			ofScale(params.global_scale);
			ofNoFill();
			ofDrawCircle(0, 0, params.screen_dims[3]);

			ofPopMatrix();
			ofPopMatrix();

			ofSetColor(255);
			ofPushMatrix();
			ofTranslate(params.plotHeight, 16);

			// PRINT DEVICE INFORMATION (CAMERA, AUDIO, MIDI) AND PARAMETERS TO SCREEN
			// ofDrawBitmapString("Frequency Domain", 0, 0);
			// plot(sound, -params.plotHeight, params.plotHeight / 2);

			ofDrawBitmapString("/// ..:: raspVISUAL ::.. STEREO_VISION 2020 (V 1.0)///", 0, 2.5 * params.plotHeight - 20);

			ofDrawBitmapString("|| VIDEO DEV:", 0, 2.5 * params.plotHeight);
			ofDrawBitmapString(params.video_dev, 0, 2.5 * params.plotHeight + 20);

			ofDrawBitmapString("|| AUDIO DEV:", 0, 2.5 * params.plotHeight + 40);
			ofDrawBitmapString(params.audio_dev, 0, 2.5 * params.plotHeight + 60);

			ofDrawBitmapString("|| MIDI DEV:", 0, 2.5 * params.plotHeight + 80);
			ofDrawBitmapString(params.midi_dev, 0, 2.5 * params.plotHeight + 100);

			ofDrawBitmapString("|| RELEASE:", 0, 2.5 * params.plotHeight + 120);
			ofDrawBitmapString(params.release, 0, 2.5 * params.plotHeight + 140);

			ofDrawBitmapString("|| SENS:", 0, 2.5 * params.plotHeight + 160);
			ofDrawBitmapString(params.sens_sound, 0, 2.5 * params.plotHeight + 180);

			ofDrawBitmapString("|| MASK:", 0, 2.5 * params.plotHeight + 200);
			if(params.set_mask)
			{
				ofDrawBitmapString("ACTIVE", 0, 2.5 * params.plotHeight + 220);
			}
			else
			{
				ofDrawBitmapString("INACTIVE", 0, 2.5 * params.plotHeight + 220);
			}

			ofPopMatrix();

			string msg = ofToString((int)ofGetFrameRate()) + " fps";
			ofDrawBitmapString(msg, params.screen_dims[0] - 80, params.screen_dims[2] - 20);

		}

	fbo.end();

}

//--------------------------------------------------------------

void Grafica::plot(float* sound, float scale, float offset) {

	float w = (float)(5*128) / 24;

	ofPushStyle();
	ofNoFill();

	for (int i = 0; i < 24; i ++){
		ofDrawRectangle(20+i*w, -2.25*scale, w, 
		sound[i]*scale);
		// ofDrawBitmapString(sound[i], 40+i*w, 2.35*plotHeight );
	}

	ofPopStyle();

}

//--------------------------------------------------------------

void Grafica::min_shader_cam(ofFbo& fbo, ofTexture &videoTexture, ofShader shader, float* sound, int* controller, Params &params){
    
    ofColor local_col;
    
	fbo.begin();

	ofPushMatrix();
	ofTranslate(params.screen_dims[1]+params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);
	ofClear(0, 0, 0, 0);

	shader.begin();

	shader.setUniform1f("brightness", params.brt_value);
	shader.setUniform1f("saturation", params.sat_value);
	shader.setUniform1f("contrast",   params.cont_value);

	videoTexture.draw(params.camPosX, params.camPosY, params.camScW, params.camScH);

	shader.end();
	
	local_col.setHsb(params.video_hue, 255, 255, 150);

	ofSetRectMode(OF_RECTMODE_CORNER);

	if(params.video_hue > 1){
		ofSetColor(local_col);
		ofFill();
		ofDrawRectangle(params.camPosX, params.camPosY, params.camScW, params.camScH);
	}

	ofPopMatrix();
	fbo.end();
}

//--------------------------------------------------------------

void Grafica::min_cam(ofFbo& fbo, ofTexture &videoTexture, float* sound, int* controller, Params &params){

	ofColor local_col;

	fbo.begin();

	ofPushMatrix();

	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofClear(0, 0, 0, 0);
	local_col.setHsb(params.video_hue, 255, 255, 150);

	videoTexture.draw(params.camPosX, params.camPosY, params.camScW, params.camScH);	
	
	// if (pixels == false) {
	// 	videoTexture.draw(0, 0, params.camWidth*params.escalaVideo, params.camHeight*params.escalaVideo);
	// }
	// else {

	// 	ofColor col;
	// 	float brillo;
	// 	float scBrillo;
	// 	float ampV;
	// 	int indice;

	// 	ofSetRectMode(OF_RECTMODE_CENTER);

	// 	for (int j = 0; j < camHeight; j += sep) {
	// 		for (int i = 0; i < camWidth; i += sep) {

	// 			indice = (i + j*camWidth) * 3;

	// 			ampV = ofMap(sound[i%16], 0.0, params.sens_sound, 0.0, pixelAudio);
	// 			col = ofColor(videoPixels[indice], videoPixels[indice + 1], videoPixels[indice + 2], ofMap(controller[2], 0, 127, 0, 255));

	// 			if(pixelShape == true){
	// 				brillo = col.getBrightness();
	// 				scBrillo = ofMap((float) brillo, 0.0, 255.0, 0.0, 3.0);
	// 				if (brillo > 80){
	// 					ofSetColor(col);
	// 					ofFill();
	// 					ofDrawRectangle(
	// 						camPos.x + i*escalaVideo,
	// 						camPos.y + j*escalaVideo,
	// 						sep*scBrillo + ampV,
	// 						sep*scBrillo + ampV
	// 					);
	// 				}
	// 			}else{
	// 				brillo = col.getBrightness();
	// 				scBrillo = ofMap(brillo, 0, 255, 0, 2);
	// 				if (brillo > 80) {
	// 					ofSetColor(col);
	// 					ofFill();
	// 					ofDrawEllipse(
	// 						camPos.x + i*escalaVideo,
	// 						camPos.y + j*escalaVideo,
	// 						sep*scBrillo + ampV,
	// 						sep*scBrillo + ampV
	// 					);
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	ofSetRectMode(OF_RECTMODE_CORNER);

	if(params.video_hue > 1){
		ofSetColor(local_col);
		ofFill();
		ofDrawRectangle(params.camPosX, params.camPosY, params.camScW, params.camScH);
	}

	ofPopMatrix();

	fbo.end();
}

//--------------------------------------------------------------
void Grafica::moon(float* sound, int* controller, Params &params) {

	float vol = float(controller[8])/255.0;
	
	float ang;
	float ang1;
	float R = 0.75*params.screen_dims[3];

	ofPushStyle();

	if(controller[5] >= 2){
		params.local_color[0].setHsb(controller[5], 200 + controller[8], 255, 150);
	}else{
		params.local_color[0].set(255, 150);
	}

	ofPushMatrix();

	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);

	ofScale(params.global_scale + vol*ofMap(sound[2], 0.25, params.sens_sound, -0.5, 0.5, true));

	if(sound[2] > 0.1) ofRotateRad(ofRandom(10.0));

	for(int i = 1; i < 128; i ++){

		params.local_color[0].a = ofMap(sound[i%24], 0.0, params.sens_sound, 50, 255, true);
		ofSetColor(params.local_color[0]);

		ang = ofRandom(i, TWO_PI);
		ang1 = ofRandom(i, TWO_PI);
		ofDrawLine(R*cos(ang), R*sin(ang), R*cos(ang1), R*sin(ang1));
		
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------
void Grafica::circs(float* sound, int* controller, Params &params) {

	float vol = float(controller[8])/255.0;

	int rad = params.screen_dims[3] / 8;
	int n_circs = 20;
	float ang = TWO_PI / float(n_circs);
	float scale;

	ofPushStyle();

	if(controller[5] >= 2){
		params.local_color[0].setHsb(controller[5], 250 + controller[8], 255, 150);
	}else{
		params.local_color[0].set(255, 150);
	}

	ofSetColor(params.local_color[0]);

	ofPushMatrix();
	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofNoFill();
	float s_t = sin((0.1 + vol) * params.g_time);

	for (int i = 1; i < 24; i++) {

		scale = ofMap(sound[i], 0.0, params.sens_sound, 0.1, 5.0, true);

		// if (sound[i] >= 0.25*params.sens_sound-vol)
		// ofRotate(ofMap(sound[i], 0.0, params.sens_sound, 0.1, i*5, true));

		ofRotate(i*s_t);

		rad *= s_t;
		
		for (int j = 0; j < n_circs; j++) {
			ofDrawCircle( i*(rad+20)*cos(j*ang), i*(rad+20)*sin(j*ang), rad + i*scale);
		}
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------
void Grafica::spiral(float* sound, int* controller, Params &params){

	float vol = float(controller[9])/255.0;
	
	int rad = 0.05*params.screen_dims[2];
	int n_circs = 24;
	float ang = TWO_PI / float(n_circs);
	float scale;
	
	ofPushStyle();
	ofNoFill();

	ofPushMatrix();
	ofTranslate(params.screen_dims[1]+params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	for (int i = 1; i < 10; i++) {

		if(controller[6] >= 2){
			params.local_color[1].setHsb(controller[6] + 5*(i%24), 250 + controller[9], 255, 150);
		}else{
			params.local_color[1].set(255, 150);
		}

		ofSetColor(params.local_color[1]);

		if (sound[2] >= 0.25*params.sens_sound) ofRotate(2 * i * params.g_time);

		ofRotate( 10*i + vol*params.g_time);
		scale = 0.1 + fmod(i + vol*params.g_time, 2.0);

		ofPushMatrix();

		ofScale(scale);

		for (int j = 0; j < n_circs; j++) {
			ofDrawCircle( i * rad * cos(j*ang), i*rad*sin(j*ang), 5 * i*sound[j]);
			ofDrawCircle(-i * rad * cos(j*ang), i*rad*sin(j*ang), 5 * i*sound[j]);
		}

		ofPopMatrix();

	}
	ofPopMatrix();
	ofPopStyle();

}

//--------------------------------------------------------------
void Grafica::rays(float* sound, int* controller, Params &params){

	float vol = float(controller[9])/255.0;
	
	int n_circs  = 128;
	float ang    = TWO_PI / float(n_circs);
	float offSet = 0.25 * params.screen_dims[2];
	float r      = 0.1 * params.screen_dims[2];
	float R      = 3.0*r;

	offSet *= 0.25 + 3.0*vol;

	ofPushStyle();
	ofPushMatrix();
	ofTranslate(params.screen_dims[1]+params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofRotate(-10 * params.g_time);

	if(controller[6] >= 2){
		params.local_color[1].setHsb(controller[6], 255, 255, 100 + 150*(10.0*sound[2]));
		ofSetColor(params.local_color[1]);
	}else{
		ofSetColor(255, 50 + 100*(10.0*sound[2]));
	}

	for (int i = 1; i <= n_circs; i++) {
		ofDrawLine(
			r*cos(i*ang), r*sin(i*ang), 
			(R + ofMap(sound[i%24], 0.0, params.sens_sound, -offSet, offSet))*cos(i*ang), 
			(R + ofMap(sound[i%24], 0.0, params.sens_sound, -offSet, offSet))*sin(i*ang));
	}

	ofPopMatrix();
	ofPopStyle();

}

//--------------------------------------------------------------

void Grafica::spectrum(float* sound, int* controller, Params &params){

	float vol = float(controller[9])/255.0;

	int sx = params.screen_dims[1]/28;
	float bandVal;
	float rectW;
	float rad;

	ofPushStyle();
	ofPushMatrix();

	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);
	
	ofNoFill();

	if(controller[6] >= 2){
		params.local_color[1].setHsb(controller[6], 255, 255, 50 + 100*sound[2]);
		ofSetColor(params.local_color[1]);
	}else{
		ofSetColor(255, 50 + 100*sound[2]);
	}

	rad = fmod((50.0 + 50.0*vol) * params.g_time, params.screen_dims[3]);

	ofSetLineWidth(5);
	if(sound[2] > 0.2){
		for(int i=1; i < 10; i++){
			ofDrawCircle(0, 0, 0.75*i*rad, 0.75*i*rad);
		}
	}

	ofSetRectMode(OF_RECTMODE_CENTER);
	ofFill();
	ofSetLineWidth(1);

	for(int i = 1; i < 24; i++) {

		rectW   = (0.5 + vol) * ofMap(sound[i], 0.0, params.sens_sound, 25, 50);
		bandVal = (0.5 + vol) * ofMap(sound[i], 0.0, params.sens_sound, 25, params.screen_dims[3]);

		ofDrawRectangle( (i-1)*sx, 0, rectW, bandVal);
		ofDrawRectangle(-(i-1)*sx, 0, rectW, bandVal);

	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------

void Grafica::triditest(float* sound, int* controller, Params &params) {

	float vol = float(controller[10])/255.0;

	int n = 5;

	if(controller[7] >= 2){
		params.local_color[2].setHsb(controller[7], 255, 255, 50 + 100*(5.0*sound[2]));
		ofSetColor(params.local_color[2]);
	}else{
		ofSetColor(255, 50 + 50*(5.0*sound[2]));
	}

	ofPushStyle();
	ofNoFill();
	ofPushMatrix();
	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofRotateY(0.25*(0.5 + vol)*params.g_time);

	if(ofRandom(1.0)< 0.25) ofSetSphereResolution(ofRandom(3,10));

	for(int i = -n; i < n; i++){
	
		ofRotateXRad(i*5);

		for(int j = -n; j < n; j++){
			ofRotateY(j*2);
			ofDrawSphere(
				i*40 + (20+15*vol)*cos(0.75*i*params.g_time),
				j*40 + (20+15*vol)*sin(0.75*j*params.g_time), 
				vol*250*sin(0.5*j*params.g_time), 
				(0.75+vol)*15*sound[(i+j)%24]
			);
		}
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------

void Grafica::tunnel(float* sound, int* controller, Params &params) {

	float vol = float(controller[10])/255.0;

	int n = 64;

	if(controller[7] >= 2){
		params.local_color[2].setHsb(controller[7], 255, 255, 50 + 100*(5.0*sound[2]));
		ofSetColor(params.local_color[2]);
	}else{
		ofSetColor(255, 50 + 50*(5.0*sound[2]));
	}

	ofPushStyle();
	ofNoFill();
	ofPushMatrix();
	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofRotateYRad((0.05 + 0.25*vol)*params.g_time);

	for(int i = 3; i < n; i ++){
		ofDrawBox(
			params.screen_dims[3]*(1.0+0.5*vol)*sound[i],
			params.screen_dims[3]*(1.0+0.5*vol)*sound[(2+i)%24],
			params.screen_dims[3]*(1.0+0.5*vol)*sound[(3+i)%24]
		);
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------
void Grafica::confetti(float* sound, int* controller, Params &params) {

	float vol = float(controller[8])/255.0;

	int n_s = 24;
	float r;

	ofPushStyle();
	ofNoFill();

	if(controller[5] >= 2){
		params.local_color[0].setHsb(controller[5], 255, 255, 100 + 150*(10.0*sound[2]));
		ofSetColor(params.local_color[0]);
	}else{
		ofSetColor(255, 50 + 100*(10.0*sound[2]));
	}

	ofPushMatrix();
	ofTranslate(params.screen_dims[1]+params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	for(int i=0; i<n_s; i ++){
		ofRotateZRad((1.0 + vol)*i);
		r = 5*i + 10*vol;
		ofDrawSphere(
			fmod(r*params.g_time, 100), 
			2*r*cos(params.g_time), 
			2*r*sin(params.g_time), 
			(50 + 50*vol)*sound[i%24]);
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------
void Grafica::tentacles(float* sound, int* controller, Params &params) {

	float vol = float(controller[10])/255.0;

	int n_r = 64;
	float v_i;
	int arms = 6;
	float ang = TWO_PI / float(arms);
	float scale = 1.5;

	float s_r = params.screen_dims[2] / float(n_r);

	ofPushStyle();
	ofPushMatrix();

	ofTranslate(params.screen_dims[1] + params.posMap[0], params.screen_dims[3]+params.posMap[1]);
	ofScale(params.global_scale);

	ofFill();

	for (int j = 0; j < arms; j++) {
		for (int i = n_r; i > 0; i--) {
			ofPushMatrix();

			scale = ofMap(sound[i%24], 0.2, 1.2, -0.5, 0.5);

			ofScale((1.2 + 2.0*vol)*scale);

			v_i = (1.0 + vol)*ofMap(i, 0, n_r, 0.1, 2.5);

			if(controller[7] >= 2){
				params.local_color[2].setHsb( controller[7] * (i % 8), 255, 255, ofMap(sound[i%24], 0.0, params.sens_sound,50, 125));
				ofSetColor(params.local_color[2]);
			}else{
				ofSetColor(60 * (i % 8), ofMap(sound[2], 0.0, 1.0, 50, 125));
			}

			ofDrawCircle((n_r - i)*s_r*cos(j*ang) * cos(v_i * params.g_time), (n_r - i)*s_r*sin(j*ang) * sin(v_i * params.g_time), 1.5*i + scale, 1.5*i + scale);
			ofDrawCircle(-(n_r - i)*s_r*cos(j*ang) * cos(v_i * params.g_time), -(n_r - i)*s_r*sin(j*ang) * sin(v_i * params.g_time), 1.5*i + scale, 1.5*i + scale);

			ofPopMatrix();
		}
	}

	ofPopMatrix();
	ofPopStyle();
}

//--------------------------------------------------------------
