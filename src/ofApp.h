#pragma once

#include "ofMain.h"
#include "ofxBeat.h"
#include "ofxMidi.h"
#include "ofxXmlSettings.h"

#include <string>

#include "Grafica.h"
#include "Params.h"

class ofApp : public ofBaseApp, public ofxMidiListener  {
	ofxBeat beat;

public:
	void setup();
	void draw();
	void update();
	void exit();
	void initControllerSetup();

	//===== VARIOUS VARIABLES =======

	void initialize_params();

	//===== MIDI CONTROLLERS =======//
	int controller[NCONT];

	void newMidiMessage(ofxMidiMessage& eventArgs);
	stringstream text;

	ofxMidiIn      midiIn;
	ofxMidiOut     midiOut;
	ofxMidiMessage midiMessage;

	void setup_midi_controller();

	// ======= CAMERA AND VISUALIZATION ALGORITHMS ======= //

	ofVideoGrabber vidGrabber;
	ofPixels videoPixels;
	ofTexture videoTexture;

	ofShader shader_cam;

	void setup_cam(Params &params);
	void update_cam();

	// ======= CAMERA AND VISUALIZATION ALGORITHMS ======= //
	
	Grafica grafica;
	Params params;

	// ============== AUDIO ANALYSIS ================ //

	/// AUDIO ANALYISIS WIHT ofxFft
	// int plotHeight, bufferSize;
	// ofxFft* fft;
	// ofMutex soundMutex;
	// vector<float> drawBins, middleBins, audioBins;
	// float meanBins[3];
	// ofSoundStream soundStream;
	// void setup_audio();
	// void audioReceived(float* input, int bufferSize, int nChannels);

	/// AUDIO ANALYISIS WIHT ofxBeat
	void audioReceived(float*, int, int);
	ofSoundStream soundStream;
	void setup_audio();

	float sound[32];
	float drums[3];

	// ============== EXPORT AND READ PARAMETERS ================ //
	void dump_params_to_file(Params &params, int* controller, bool rbt_flag );
	void read_params_from_file(Params* params, int* controller);

	// ============== FBO INSTANCES ================ //

	ofFbo fboVis; // fbo Mixer visuales
	ofFbo fboCam; // fbo Camara
	ofFbo fboMixer; // fbo Mixer Vis-Cam
	ofFbo fboMaster; // fbo Master
	ofFbo fboGroup[2]; // fbo por grupo de visuales

	// ============== UPDATE FUNCTIONS ================ //
	void updateScenes(bool* sc_flags, int scene);
	void updateRDMScenes(vector<int>&, int scene);

	void update_fbo(Grafica& grafica, ofFbo& fbo , float* sound, int* controller, Params &params);
	void update_fbo_scene(Grafica& grafica, ofFbo& fbo, int scene, float* sound, int* controller, Params &params);

	// ============== BLENF FBOS FUNCTIONS ================ //
	void mixVisCam(ofFbo& fboVis, ofFbo& fbo1, ofFbo& fbo2, ofBlendMode blendMode);
	void mixGroupVis(ofFbo& fboVis, ofFbo& fbo1, ofFbo& fbo2, float opaTrans, ofBlendMode blendMode);
	
	// ============== RANDOM SCENE SELECTION FUNCTIONS ================ //
	void randomManTrans(bool& manual_trans, int pushed_Scene);
	void random_nextScene(vector<int>& active_scenes, int& curr_Scene, int& next_Scene);
	void display_active_scenes(vector<int>& active_scenes);

	// ============== TEST OR DEPECRATED ================ //
	// void update_fbo(Grafica& grafica, ofFbo& fbo ,vector<float>& sound, float* meanBins, int* controller, Params &params);
	// void update_fbo_scene(Grafica& grafica, ofFbo& fbo, int scene, vector<float>& sound, float* meanBins, int* controller, Params &params);
};
