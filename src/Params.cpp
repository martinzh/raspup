// =========================== //
//  Params.cpp
//  Various Global Parameters
// =========================== //

#include "Params.h"

//--------------------------------------------------------------

Params::Params(){

	//======================================//

	global_scale = 1.0;
	scaled_half_screen_inv = 0.0;

	screen_dims[0] = 0;
	screen_dims[1] = 0;
	screen_dims[2] = 0;
	screen_dims[3] = 0;

	posMap[0] = 0;
	posMap[1] = 0;

	manual_trans = false;

	//======================================//

	// SOUND "SENSITIVITY" 
	sens_sound = 1.5;

	// RELEASE UPDATE CONTROL
	release = 0.9;

	//======================================//

	help        = false;
	mapeaMidi   = false;
	mapeaTeclas = false;
	exit_app    = false;
	shutdown    = false;
	reboot      = false;
	videoActive = false;
	set_mask    = true;
	
	//======================================//

	record_scenes = false;
	curr_Scene = 0;
	next_Scene = 0;

	//======================================//


	jetzt = 0.0;
	time0 = 0.0;
	trans_time = 2.0;

	//======================================//

	// camWidth =  320;
	// camHeight = 240;

	camWidth =  640;
	camHeight = 480;

	escalaVideo = 1.0;

	camPosX = 0;
	camPosY = 0;

	camScW = 0;
	camScH = 0;

	brt_value = 0.5; // Multiplicador de brillo
	sat_value = 0.5; // Multiplicador de saturacion
	cont_value = 0.5; // Multiplicador de contraste

	n_sVal = 0; // Valor nuevo de saturacion del pixel
	n_bVal = 0; // Valor nuevo de brillo del pixel

	// Multiplicador de valor de color "glitch"
	mod_value = 1.0;

	// Hue mascarilla de video
	video_hue = 0;

	mask_angle = ofDegToRad(360 / 24);

	//======================================//

	plotHeight = 128;

	test_hue = 1; // Hue configuracion

	opaFbo    = 0.0;
	opaGral   = 0.0;
	opaTrans  = 1.0;
	
	//======================================//


	for(size_t i = 0; i<7; i++){
		local_color[i].setHsb(255, 255, 255, 150);
	}

	//======================================//

}

//--------------------------------------------------------------