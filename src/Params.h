// =========================== //
//  Params.h
//  Various Global Parameters
// =========================== //

#ifndef Params_h
#define Params_h

#include <stdio.h>
#include "ofMain.h"

	class Params
{

public:
	
	//======================================//

	
	ofVec2f posMap;         //Position of center to display content
	ofColor local_color[7]; //Color of each scene
	ofColor aux_color;      // Auxiliar color
	
	//======================================//
	
	bool record_scenes;

	//======================================//

	int  screen_dims[4]; // Screen's dimensions
	bool sc_flags[9];    // Active/Inactive scenes flags
	vector<int> activeScenes; // Active scenes to select from when random

	//======================================//
	
	int test_hue; //Hue configuracion
	float g_time; //Current time from app
	float jetzt;  //Right now time
	float time0;  //Reference time

	float trans_time; // Transition time in seconds

	int curr_Scene; // Current active scene
	int next_Scene; // Next active scene
	
	//======================================//

	float opaFbo;   //ofbo OPACITY
	float opaGral;  //General Alpha
	float opaTrans; // Alpha for transitions between scenes

	float release;
	float sens_sound;
	float global_scale;
	float scaled_half_screen_inv;

	float mask_angle;
	
	//======================================//

	bool videoActive;

	int camWidth;
	int camHeight;

	int camPosX;
	int camPosY;

	int camScW;
	int camScH;

	int plotHeight;

	float escalaVideo;
	
	float brt_value;  //Multiplicador de saturacion
	float sat_value;  //Multiplicador de saturacion
	float cont_value; //Multiplicador de contraste

	int n_sVal; //Valor nuevo de saturacion del pixel
	int n_bVal; //Valor nuevo de brillo del pixel

	float mod_value; // Multiplicador de valor de color "glitch"

	int video_hue; // Hue mascarilla de video

	//======================================//
	
	string audio_dev; //Audio Device Name
	string midi_dev;  //MIDI Device Name
	string video_dev; //Video Device Name

	//======================================//
	
	bool manual_trans;

	bool help;
	bool mapeaMidi;
	bool mapeaTeclas;
	
	bool exit_app;
	bool shutdown;
	bool reboot;

	bool set_mask;

	//======================================//
	
	ofBlendMode blendModeCam;
	ofBlendMode blendModeVis;
	ofBlendMode blendModeMix;
	bool blendMix;
	
	//======================================//
	
	Params();

	//======================================//

	private:
	};

	#endif /* Params_h */

// =========================== //
