//======================================//

#include "ofApp.h"

//======================================//

void ofApp::setup()
{
	// shader_cam.load("shadersGL2/shader");
	// shader_cam.load("shadersES2/shader");

	ofSetCircleResolution(40);
	ofHideCursor();

	initialize_params();
	setup_cam(params);
	setup_audio();
	setup_midi_controller();

	fboVis.allocate(ofGetWidth()     , ofGetHeight(), GL_RGBA);
	fboCam.allocate(ofGetWidth()     , ofGetHeight(), GL_RGBA);
	fboMixer.allocate(ofGetWidth()   , ofGetHeight(), GL_RGBA);
	fboMaster.allocate(ofGetWidth()  , ofGetHeight(), GL_RGBA);
	fboGroup[0].allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
	fboGroup[1].allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);

	ofBackground(0, 0, 0);

	read_params_from_file(&params, controller);

	params.time0 = ofGetElapsedTimef();
}

//======================================//

void ofApp::update()
{
	///=========== BEAT UPDATE ===========///
	beat.update(ofGetElapsedTimeMillis());

	params.g_time = ofGetElapsedTimef();

	///=========== CAMERA UPDATE ===========///
	update_cam();

	if(!params.mapeaMidi)
	{
		if(params.manual_trans)
		{
			///=========== UPDATE VISUALS ===========///
			update_fbo(grafica, fboVis, sound, controller, params);
		}
		else
		{
			///=========== MIX GROUP FBO'S ===========///
			params.jetzt = params.g_time - params.time0;

			if(params.jetzt > params.trans_time - 0.1)
			{
				params.time0 = params.g_time;
				random_nextScene(params.activeScenes, params.curr_Scene, params.next_Scene);
			}

			params.opaTrans = 0.1 + params.jetzt/params.trans_time;

			update_fbo_scene(grafica, fboGroup[0], params.curr_Scene, sound, controller, params);
			update_fbo_scene(grafica, fboGroup[1], params.next_Scene, sound, controller, params);

			mixGroupVis(fboVis, fboGroup[0], fboGroup[1], params.opaTrans, params.blendModeVis);
		}
	}
	else
	{
		update_fbo_scene(grafica, fboGroup[0],  9, sound, controller, params);
		update_fbo_scene(grafica, fboGroup[1], -1, sound, controller, params);

		mixGroupVis(fboVis, fboGroup[0], fboGroup[1], 0.0, OF_BLENDMODE_ALPHA);
	}

	if(params.record_scenes) display_active_scenes(params.activeScenes);

	mixGroupVis(fboMixer, fboVis, fboCam, params.opaFbo, params.blendModeCam);
	grafica.master(fboMaster, sound, params);
}

//======================================//

void ofApp::draw() {

	// glEnable (GL_BLEND);
	// glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);

	// ofSetColor(255, 255);
	// ofFill();
	fboMixer.draw(0,0);
	
	// ofSetColor(255, 255);
	// ofFill();
	fboMaster.draw(0, 0);

}

//======================================//

void ofApp::audioReceived(float* input, int bufferSize, int nChannels) {

	beat.audioReceived(input, bufferSize, nChannels);

	for(int i = 0; i < NSOUND; i ++){
		sound[i] *= params.release;
		if(sound[i] < beat.getBand(i)) sound[i] = beat.getBand(i);
	}

	drums[0] *= params.release; if(drums[0] < beat.kick())  drums[0] = beat.kick();
	drums[1] *= params.release; if(drums[1] < beat.snare()) drums[1] = beat.snare();
	drums[2] *= params.release; if(drums[2] < beat.hihat()) drums[2] = beat.hihat();

}

//======================================//

void ofApp::setup_cam(Params &params){

	//get back a list of devices.
	vector<ofVideoDevice> devices = vidGrabber.listDevices();

	for(size_t i = 0; i < devices.size(); i++){
		if(devices[i].bAvailable){
			//log the device
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
			}else{
			//log the device and note it as unavailable
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}

	vidGrabber.setDeviceID(0);
	params.video_dev.assign(devices[0].deviceName);

	vidGrabber.setDesiredFrameRate(30);
	vidGrabber.initGrabber(params.camWidth, params.camHeight);

	videoPixels.allocate(params.camWidth, params.camHeight, OF_PIXELS_RGB);
	videoTexture.allocate(videoPixels);
	ofSetVerticalSync(true);

	params.videoActive = true;
}


//======================================//

void ofApp::update_cam(){

	// glitchShaderCam.setFx(OFXPOSTGLITCH_CR_HIGHCONTRAST, true);
	vidGrabber.update();
	if (vidGrabber.isFrameNew()) {
		ofPixels & pixels = vidGrabber.getPixels();
		for (int i = 0; i < pixels.size(); i += 3)
		{
			params.aux_color.set(
				pixels[i]   * params.mod_value, 
				pixels[i+1] * params.mod_value, 
				pixels[i+2] * params.mod_value);

			params.n_sVal = (int) params.aux_color.getSaturation() * params.sat_value;
			params.aux_color.setSaturation(params.n_sVal);

			videoPixels[ i ] = params.aux_color.r; // R
			videoPixels[i+1] = params.aux_color.g; // G
			videoPixels[i+2] = params.aux_color.b; // B
		}
		videoTexture.loadData(videoPixels);
	}
	// grafica.min_shader_cam(fboCam, videoTexture, shader_cam, sound, controller, params);
	grafica.min_cam(fboCam, videoTexture, sound, controller, params);

}

//======================================//

// SETUP AUDIO ofxBeat

void ofApp::setup_audio(){

	ofSoundStreamSettings settings;

	auto devices = soundStream.getMatchingDevices("default");
	if (!devices.empty())
	{
	settings.setInDevice(devices[0]);
	}

	params.audio_dev.assign("default");

	settings.setInListener(this);
	settings.sampleRate = 44100;
	settings.numOutputChannels = 0;
	settings.numInputChannels = 2;
	settings.bufferSize = beat.getBufferSize();
	soundStream.setup(settings);
}

///===========================================///
///=========== MIDI INITIALIZATION ===========///

void ofApp::setup_midi_controller()
{

	vector<std::string> midiPorts = midiIn.getInPortList();
	std::string controllerName = "nano";
	int dev_num;

	for(int i = 0; i < midiIn.getNumInPorts(); i ++){
		std::size_t found = midiPorts[i].find(controllerName);
		if( found != string::npos){
			cout << midiPorts[i] << endl;
			dev_num = i;
		}
	}

	midiIn.openPort(dev_num);
	midiOut.openPort(dev_num);
	params.midi_dev.assign(midiPorts[dev_num]);

	midiIn.addListener(this);  // add ofApp as a listener

}

//======================================//

void ofApp::initialize_params(){

	params.screen_dims[0] = ofGetWidth();
	params.screen_dims[1] = ofGetWidth()/2;
	params.screen_dims[2] = ofGetHeight();
	params.screen_dims[3] = ofGetHeight()/2;

	params.escalaVideo = float(params.screen_dims[2]) / float(params.camHeight);
	printf("Computed video Scale: %f\n", params.escalaVideo);

	params.camPosX = -params.camWidth*params.escalaVideo/2;
	params.camPosY = -params.camHeight*params.escalaVideo/2;

	params.camScW = params.camWidth*params.escalaVideo;
	params.camScH = params.camHeight*params.escalaVideo;

}

//======================================//

void ofApp::exit() {

	for (int i = 0; i < 8; i++) {
		midiOut.sendControlChange(1, i, 0);
		midiOut.sendControlChange(1, i+10, 0);
		midiOut.sendControlChange(1, i+20, 0);
		midiOut.sendControlChange(1, i+60, 0);
	}

	midiIn.closePort();
	midiOut.closePort();
	midiIn.removeListener(this);

}

//======================================//

void ofApp::updateScenes(bool* sc_flags, int scene){

	if (sc_flags[scene] == true){
		sc_flags[scene] = false;
		midiOut.sendControlChange(1, scene, 0);
	} else {
		sc_flags[scene] = true;
		midiOut.sendControlChange(1, scene, 127);
	}

}

//======================================//

void ofApp::update_fbo(Grafica& grafica, ofFbo& fbo , float* sound, int* controller, Params &params){

	fbo.begin();

	ofClear(0,0,0,0);
	
	ofEnableBlendMode(params.blendModeVis);

	if(params.sc_flags[0]) grafica.circs(   sound, controller, params);
	if(params.sc_flags[1]) grafica.moon(    sound, controller, params);
	if(params.sc_flags[2]) grafica.confetti(sound, controller, params);

	if(params.sc_flags[3]) grafica.rays(    sound, controller, params);
	if(params.sc_flags[4]) grafica.spectrum(sound, controller, params);
	if(params.sc_flags[5]) grafica.spiral(  sound, controller, params);

	if(params.sc_flags[6]) grafica.triditest(sound, controller, params);
	if(params.sc_flags[7]) grafica.tunnel(   sound, controller, params);
	if(params.sc_flags[8]) grafica.tentacles(sound, controller, params);

	ofDisableBlendMode();

	fbo.end();
}

//======================================//

void ofApp::update_fbo_scene(Grafica& grafica, ofFbo& fbo, int scene, float* sound, int* controller, Params &params){

	fbo.begin();

	ofClear(0,0,0,0);

	ofEnableBlendMode(params.blendModeVis);

	switch (scene) {
		case 0: grafica.circs(   sound, controller, params); break;
		case 1: grafica.moon(    sound, controller, params); break;
		case 2: grafica.confetti(sound, controller, params); break;

		case 3: grafica.rays(    sound, controller, params); break;
		case 4: grafica.spectrum(sound, controller, params); break;
		case 5: grafica.spiral(  sound, controller, params); break;

		case 6: grafica.triditest(sound, controller, params); break;
		case 7: grafica.tunnel(   sound, controller, params); break;
		case 8: grafica.tentacles(sound, controller, params); break;
		case 9: grafica.config(   sound, controller, params); break;
		default: break;
	}

	ofDisableBlendMode();

	fbo.end();
}

//======================================//

void ofApp::random_nextScene(vector<int>& active_scenes, int& curr_Scene, int& next_Scene){

	int next_scene_id;
	int temp_Scene = curr_Scene;
	int num_active_scenes = active_scenes.size();
	
	curr_Scene = next_Scene;

	if(num_active_scenes > 1)
	{
		do{ 
			next_scene_id = ofRandom(num_active_scenes);
			next_Scene    = active_scenes[next_scene_id];
		}while(next_Scene == temp_Scene);

		if(params.record_scenes == false)
		{
			midiOut.sendControlChange(1, curr_Scene, 0);
			midiOut.sendControlChange(1, next_Scene, 127);
		}
	}	
}

//======================================//

void ofApp::updateRDMScenes(vector<int>& activeScenes, int scene){

	if(activeScenes.size() > 0){
		// Find given element in vector
		auto it = std::find(activeScenes.begin(), activeScenes.end(), scene);

		if (it != activeScenes.end()){
			activeScenes.erase(it);
			midiOut.sendControlChange(1, scene, 0);
		} else {
			activeScenes.push_back(scene);
			midiOut.sendControlChange(1, scene, 127);
		}
		
	}else{
			activeScenes.push_back(scene);
			midiOut.sendControlChange(1, scene, 127);
	}

}

//======================================//

void ofApp::display_active_scenes(vector<int>& active_scenes){
	
	for(int i = 0; i < active_scenes.size(); i ++){
		midiOut.sendControlChange(1, active_scenes[i], 127);
	}

}

//======================================//

void ofApp::randomManTrans(bool& manual_trans, int pushed_Scene){
	
	if (manual_trans == false) {

		for(int i = 0; i < NVIS; i ++){
			params.sc_flags[i] = false;
			midiOut.sendControlChange(1, i, 0);
		}

		params.sc_flags[pushed_Scene] = true;
		midiOut.sendControlChange(1, pushed_Scene, 127);

		manual_trans = true;
		midiOut.sendControlChange(1, 9, 0);
	}

}

//======================================//

void ofApp::mixGroupVis(ofFbo& fboVis, ofFbo& fbo1, ofFbo& fbo2, float opaTrans, ofBlendMode blendMode){

	fboVis.begin();
	ofClear(0,0,0,0);

	// glEnable (GL_BLEND);
	// glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);

	ofEnableBlendMode(blendMode);

	/// CURRENT SCENE
	ofSetColor(255, 255*(1-opaTrans));
	ofFill();
	fbo1.draw(0,0);

	/// NEXT SCENE
	ofSetColor(255, 255*opaTrans);
	ofFill();
	fbo2.draw(0,0);

	ofDisableBlendMode();
	// glDisable(GL_BLEND);

	fboVis.end();

}

//======================================//

void ofApp::mixVisCam(ofFbo& fboVis, ofFbo& fbo1, ofFbo& fbo2, ofBlendMode blendMode){

	fboVis.begin();
	ofClear(0,0,0,0);

	// glEnable (GL_BLEND);
	// glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);

	ofEnableBlendMode(blendMode);

	/// CURRENT SCENE
	ofSetColor(255, 255);
	ofFill();
	fbo1.draw(0,0);

	/// NEXT SCENE
	ofSetColor(255, 255);
	ofFill();
	fbo2.draw(0,0);

	ofDisableBlendMode();
	// glDisable(GL_BLEND);

	fboVis.end();

}

//======================================//

void ofApp::initControllerSetup(){

	///=========== SCENE FLAGS ===========///
	for (int i = 0; i < NVIS; i++) {
		params.sc_flags[i] = false;
	}

	///=========== CONTROLLER ARRAY ===========///
	for (int i = 0; i < NCONT; i++) {
		controller[i] = 0;
	}

	///=========== FANCY INIT LIGHTS ===========///
	for (int i = 0; i < 24; i++) {
		midiOut.sendControlChange(1, i, 0);
		midiOut.sendControlChange(1, i, 127);
	}

	sleep(1);

	for (int i = 0; i < 24; i++) {
		midiOut.sendControlChange(1, i, 0);
	}

	///=========== BLEND MODE DEFAULT ===========///
	midiOut.sendControlChange(1, 23, 127);
	params.blendModeCam = OF_BLENDMODE_SCREEN;

	midiOut.sendControlChange(1, 14, 127);
	params.blendModeVis = OF_BLENDMODE_SCREEN;

	///=========== START WITH CONFIG SCENE ===========///
	params.mapeaMidi = true; // 
	midiOut.sendControlChange(1, 63, 127);

	///=========== START MANUAL MODE ===========///
	params.manual_trans = true;
	midiOut.sendControlChange(1, 9, 0);

	///=========== ALL SCENES FOR RANDOM AS DEFAULT ===========///
	for(int i = 0; i < NVIS; i++){
		params.activeScenes.push_back(i);
	}

}

//======================================//

void ofApp::newMidiMessage(ofxMidiMessage& msg) {

	///=========== ACTIVATE SETUP SCENE ===========///
	if(msg.control == 63)
	{
		params.mapeaMidi = !params.mapeaMidi;
		midiOut.sendControlChange(1, 63, 127*params.mapeaMidi);
	}

	if(params.mapeaMidi) ///=========== WHEN IN SETUP... ===========///
	{
		switch (msg.control)
		{
			///=========== SHOW HELP INFO ===========///
			// case 64: params.help = !params.help; break;

			///=========== SET MASK ===========///
			case 64:
				params.set_mask = !params.set_mask; 
				midiOut.sendControlChange(1, 64, 127*params.set_mask);
			break;

			///=========== EXIT APP ===========///
			case 46: 
				dump_params_to_file(params, controller, false);
				ofExit(); 
			break;
			
			///=========== RASPBERRY PI (REBOOT) ===========///
			case 58:
				dump_params_to_file(params, controller, true);
				system("sudo shutdown -r now"); 
			break;
			
			///=========== RASPBERRY PI (SHUTDOWN) ===========///
			case 59: 
				dump_params_to_file(params, controller, false);
				system("sudo shutdown -h now"); 
			break;

			///=========== TESTS HUE ADJUST ===========///
			case 52: params.test_hue = ofMap(msg.value, 0, 127, 1, 255); break;

			///=========== X SCREEN ADJUST ===========///
			case 31:
				params.posMap.x = ofMap(msg.value, 0, 127, -params.screen_dims[1], params.screen_dims[1], true);
			break;

			///=========== Y SCREEN ADJUST ===========///
			case 32:
				params.posMap.y = ofMap(msg.value, 0, 127, -params.screen_dims[3], params.screen_dims[3], true);
			break;

			///=========== ADJUST GLOBAL SCALE ===========///
			case 30: params.global_scale = ofMap(float(msg.value), 0.0, 127.0, 0.5, 2.0); break;

			///=========== RELEASE ADJUST ===========///
			case 50: params.release = ofMap(float(msg.value), 0.0, 127.0, 0.85, 0.99); break;

			///=========== AUDIO SENSITIVITY ===========///
			case 51: params.sens_sound = ofMap(float(msg.value), 0.0, 127.0, 0.1, 2.0); break;

			///=========== CAMERA FADER ===========///
			case 57:
				params.opaFbo = ofMap(float(msg.value), 0.0, 127.0, 0.0, 1.0);
				controller[2] = msg.value;
			break;
			
			///=========== MASTER ALPHA ===========///
			case 37:
				params.opaGral = ofMap(float(msg.value), 0.0, 127.0, 1.0, 0.0);
				controller[3] = msg.value;
			break;

		}
	}
	else if (params.record_scenes == false) ///=========== PERFORMANCE MODE ===========///
	{	
		switch (msg.control)
		{
			
			///=========== REBOOT AND SAVE SYSTEM STATE ===========///
			case 58:
				dump_params_to_file(params, controller, true);
				// ofExit();
				system("sudo shutdown -r now"); // RPI
			break;

			///=========== SWITCH BETWEEN MANUAL AND RANDOM MODE ===========///
			case 9:
				params.manual_trans = !params.manual_trans;
				midiOut.sendControlChange(1, 9, 127*!params.manual_trans);

				params.time0 = params.g_time;

				if (params.manual_trans == false){

					for(int i = 0; i < NVIS; i ++){
						midiOut.sendControlChange(1, i, 0);
						params.sc_flags[i] = false;
					}

				}else{
					
					for(int i = 0; i < NVIS; i ++){
						midiOut.sendControlChange(1, i, 0);
						params.sc_flags[i] = false;
					}

					params.sc_flags[params.curr_Scene] = true;
					params.sc_flags[params.next_Scene] = true;

					midiOut.sendControlChange(1, params.curr_Scene, 127);
					midiOut.sendControlChange(1, params.next_Scene, 127);
				}
			break;

			///=========== ENTER RECORD ACTIVE SCENES (FOR RANDOM) MODE ===========///
			case 10:
				if(!params.manual_trans)
				{
					params.record_scenes = !params.record_scenes;
					midiOut.sendControlChange(1, 10, 127*params.record_scenes);
					for (size_t i = 0; i < NVIS; i++) {midiOut.sendControlChange(1, i, 0);}
				}

			break;

			///=========== TURNS OFF ALL ACTIVE VISUALS ===========///
			case 67:
				for (size_t i = 0; i < NVIS; i++) {
					params.sc_flags[i] = false;
					midiOut.sendControlChange(1, i, 0);
				}
			break;

			///=========== SWITCH BLEND MODES (VISUALS AND VIDEO) ===========///
			case 12:
				params.blendModeVis = OF_BLENDMODE_ALPHA;
				midiOut.sendControlChange(1, 12, 127);
				midiOut.sendControlChange(1, 13, 0);
				midiOut.sendControlChange(1, 14, 0);
				break;

			case 13:
				params.blendModeVis = OF_BLENDMODE_ADD;
				midiOut.sendControlChange(1, 12, 0);
				midiOut.sendControlChange(1, 13, 127);
				midiOut.sendControlChange(1, 14, 0);
				break;

			case 14:
				params.blendModeVis = OF_BLENDMODE_SCREEN;
				midiOut.sendControlChange(1, 12, 0);
				midiOut.sendControlChange(1, 13, 0);
				midiOut.sendControlChange(1, 14, 127);
				break;

			case 21:
				params.blendModeCam = OF_BLENDMODE_ALPHA;
				midiOut.sendControlChange(1, 21, 127);
				midiOut.sendControlChange(1, 22, 0);
				midiOut.sendControlChange(1, 23, 0);
				break;

			case 22:
				params.blendModeCam = OF_BLENDMODE_ADD;
				midiOut.sendControlChange(1, 21, 0);
				midiOut.sendControlChange(1, 22, 127);
				midiOut.sendControlChange(1, 23, 0);
				break;

			case 23:
				params.blendModeCam = OF_BLENDMODE_SCREEN;
				midiOut.sendControlChange(1, 21, 0);
				midiOut.sendControlChange(1, 22, 0);
				midiOut.sendControlChange(1, 23, 127);
				break;

			///=========== SET ACTIVE SCENES (MANUAL MODE AND RDM -> MAN TRANSITION)===========///
			case 0:
				updateScenes(params.sc_flags, 0);
				randomManTrans(params.manual_trans, 0);
			break;
			case 1:
				updateScenes(params.sc_flags, 1);
				randomManTrans(params.manual_trans, 1);
			break;
			case 2:
				updateScenes(params.sc_flags, 2);
				randomManTrans(params.manual_trans, 2);
			break;
			case 3:
				updateScenes(params.sc_flags, 3);
				randomManTrans(params.manual_trans, 3);
			break;
			case 4:
				updateScenes(params.sc_flags, 4);
				randomManTrans(params.manual_trans, 4);
			break;
			case 5:
				updateScenes(params.sc_flags, 5);
				randomManTrans(params.manual_trans, 5);
			break;
			case 6:
				updateScenes(params.sc_flags, 6);
				randomManTrans(params.manual_trans, 6);
			break;
			case 7:
				updateScenes(params.sc_flags, 7);
				randomManTrans(params.manual_trans, 7);
			break;
			case 8:
				updateScenes(params.sc_flags, 8);
				randomManTrans(params.manual_trans, 8);
			break;

			///=========== HUE VISUALS ===========///
			case 30: controller[5] = ofMap(msg.value, 0, 127, 0, 255); break;
			case 31: controller[6] = ofMap(msg.value, 0, 127, 0, 255); break;
			case 32: controller[7] = ofMap(msg.value, 0, 127, 0, 255); break;

			///=========== INTENSITY VISUALS ===========///
			case 50: controller[8]  = ofMap(msg.value, 0, 127, 0, 255); break;
			case 51: controller[9]  = ofMap(msg.value, 0, 127, 0, 255); break;
			case 52: controller[10] = ofMap(msg.value, 0, 127, 0, 255); break;

			///=========== TRANSITION TIME VISUALS ===========///
			case 33: params.trans_time = ofMap(float(msg.value), 0.0, 127.0, 10.0, 0.1); break;

			///=========== CAMERA FADER ===========///
			case 57:
				params.opaFbo = ofMap(float(msg.value), 0.0, 127.0, 0.0, 1.0);
				controller[2] = msg.value;
			break;

			///=========== MASTER ALPHA ===========///
			case 37:
				params.opaGral = ofMap(float(msg.value), 0.0, 127.0, 1.0, 0.0);
				controller[3] = msg.value; //? ESTE DONDE SE USA?
			break;

			///=========== CAMERA EFFECTS ===========///
			case 36: params.video_hue  = ofMap(msg.value       , 0  , 127  , 0  , 255); break;
			case 35: params.mod_value = ofMap(float(msg.value), 0.0, 127.0, 1.0, 10.0); break;

			case 54: params.brt_value  = ofMap(float(msg.value), 0.0, 127.0, 0.1, 1.5); break;
			case 55: params.cont_value = ofMap(float(msg.value), 0.0, 127.0, 0.5, 5.0); break;
			case 56: params.sat_value  = ofMap(float(msg.value), 0.0, 127.0, 0.1, 5.0); break;

			// case 18:
			// 	grafica.pixels = !grafica.pixels;
			// 	midiOut.sendControlChange(1, 18, 127*grafica.pixels);
			// 	break;
			// case 19:
			// 	grafica.pixelShape = !grafica.pixelShape;
			// 	midiOut.sendControlChange(1, 19, 127*grafica.pixelShape);
			// 	break;
			// case 20:
			// 	grafica.shaderFXCam = !grafica.shaderFXCam;
			// 	// if(videoActive) glitchShaderCam.setFx(OFXPOSTGLITCH_TWIST, grafica.shaderFXCam);
			// 	midiOut.sendControlChange(1, 20, 127*grafica.shaderFXCam);
			// 	break;
			// case 10:
			// 	grafica.shaderFXVis = !grafica.shaderFXVis;
			// 	// glitchShaderVis.setFx(OFXPOSTGLITCH_TWIST, grafica.shaderFXVis);
			// 	midiOut.sendControlChange(1, 10, 127*grafica.shaderFXVis);
			// 	break;

			// case 56: grafica.pixelAudio = ofMap(msg.value, 0, 127, 0, 50); break;

			///=========== CAMERA PIXEL RESOLUTION ===========///
			// case 36: sep = ofMap(msg.value, 0, 127, 3, 15); break;
			
			// case 60:
			// 	if(params.videoActive){
			// 		params.videoActive = false;
			// 		if(vidGrabber.isInitialized()){
			// 			vidGrabber.close();
			// 		}
			// 		ofSleepMillis(2000);
			// 		setup_cam(params);
			// 	}
			// break;

		}
	}
	else ///=========== SETUP RANDOM ACTIVE SCENES MODE ===========///
	{
		switch(msg.control){
			
			///=========== TOGGLE SET SCENES TO CHOOSE WHEN RANDOM ===========///
			case 10:
			params.record_scenes = !params.record_scenes;
			midiOut.sendControlChange(1, 10, 127*params.record_scenes);
			if(!params.record_scenes)
			{
				for (size_t i = 0; i < NVIS; i++) {midiOut.sendControlChange(1, i, 0);}
			} 
			break;
			
			///=========== SET ACTIVE SCENES ===========///
			case 0: updateRDMScenes(params.activeScenes, 0); break;
			case 1: updateRDMScenes(params.activeScenes, 1); break;
			case 2: updateRDMScenes(params.activeScenes, 2); break;
			case 3: updateRDMScenes(params.activeScenes, 3); break;
			case 4: updateRDMScenes(params.activeScenes, 4); break;
			case 5: updateRDMScenes(params.activeScenes, 5); break;
			case 6: updateRDMScenes(params.activeScenes, 6); break;
			case 7: updateRDMScenes(params.activeScenes, 7); break;
			case 8: updateRDMScenes(params.activeScenes, 8); break;

		}
	}
}

//======================================//

void ofApp::dump_params_to_file(Params &params, int* controller, bool rbt_flag)
{
	ofxXmlSettings XML;
	
	// FROM REBOOT FLAG
	XML.setValue("params:reboot_flag"   , rbt_flag);
	
	// AUDIO PARAMETERS
	XML.setValue("params:sens_sound"   , params.sens_sound);
	XML.setValue("params:release"      , params.release);
	
	// MASK AND ACTIVE SCENE
	XML.setValue("params:mapeaMidi"    , params.mapeaMidi);
	XML.setValue("params:set_mask"     , params.set_mask);

	// BLEND MODES
	XML.setValue("params:blendModeCam", params.blendModeCam);
	XML.setValue("params:blendModeVis", params.blendModeVis);
	XML.setValue("params:blendModeMix", params.blendModeMix);

	// TRANSITION STATE
	XML.setValue("params:manual_trans" , params.manual_trans);
	XML.setValue("params:curr_Scene"   , params.curr_Scene);
	XML.setValue("params:next_Scene"   , params.next_Scene);
	XML.setValue("params:trans_time"   , params.trans_time);
	
	// CAMERA PARAMETERS
	XML.setValue("params:camPosX"     , params.camPosX);
	XML.setValue("params:camPosY"     , params.camPosY);
	XML.setValue("params:escalaVideo" , params.escalaVideo);

	XML.setValue("params:mod_value"   , params.mod_value);
	XML.setValue("params:brt_value"   , params.brt_value);
	XML.setValue("params:sat_value"   , params.sat_value);
	XML.setValue("params:cont_value"  , params.cont_value);
	XML.setValue("params:video_hue"   , params.video_hue);
	XML.setValue("params:scaled_half_screen_inv", params.scaled_half_screen_inv);

	// X, Y POSITION OF CENTER OF SCREEN
	XML.setValue("params:posMapX"      , params.posMap.x);
	XML.setValue("params:posMapY"      , params.posMap.y);
	XML.setValue("params:global_scale" , params.global_scale);

	// FBO ALPHA VALUES
	XML.setValue("params:opaFbo"  , params.opaFbo);
	XML.setValue("params:opaGral" , params.opaGral);
	XML.setValue("params:opaTrans", params.opaTrans);

	// LOCAL COLORS
    XML.addTag("local_color");
	XML.pushTag("local_color");
	for (int i = 0; i < 7; i++)
	{
		XML.addTag("color");
        XML.pushTag("color", i);
        XML.addValue("R", params.local_color[i].r);
        XML.addValue("G", params.local_color[i].g);
        XML.addValue("B", params.local_color[i].b);
        XML.addValue("A", params.local_color[i].a);
        XML.popTag();
	}
	XML.popTag();

	// ACTIVE SCENES
	XML.addTag("active_scenes");
	XML.pushTag("active_scenes");
	for (int i = 0; i < 9; i++)
	{
		XML.addTag("scene");
		XML.pushTag("scene", i);
		XML.addValue("val", params.sc_flags[i]);
		XML.popTag();
	}
	XML.popTag();

	// RANDOM SCENES
	XML.addTag("random_scenes");
	XML.pushTag("random_scenes");
	for (int i = 0; i < params.activeScenes.size(); i++)
	{
	XML.addTag("scene");
		XML.pushTag("scene", i);
		XML.addValue("val", params.activeScenes[i]);
		XML.popTag();
	}
	XML.popTag();

	// MIDI CONTROLLER VALUES
	XML.addTag("controller");
	XML.pushTag("controller");
	for (int i = 0; i < NCONT; i++)
	{
		XML.addTag("value");
		XML.pushTag("value", i);
		XML.addValue("c_val", controller[i]);
		XML.popTag();
	}
	XML.popTag();

	// SAVE SETTINGS FILE
	XML.saveFile("saved_state.xml");
	printf("Exported settings to XML file\n");

}

//======================================//

void ofApp::read_params_from_file(Params* params, int* controller)
{
	ofxXmlSettings XML;
	std::string filename = "saved_state.xml";

	if(XML.loadFile(filename))
	{
		printf("Found params file\n");
	
		XML.pushTag("params");
		
		bool rbt_flag = XML.getValue("reboot_flag", true);
		if(rbt_flag == true)
		{
			// AUDIO PARAMETERS
			params->release    = XML.getValue("release",    0.0);
			params->sens_sound = XML.getValue("sens_sound", 0.0);

			// MASK AND ACTIVE SCENE
			params->mapeaMidi = XML.getValue("mapeaMidi", true);
			params->set_mask = XML.getValue("set_mask",   true);

			// TRANSITION STATE
			params->manual_trans = XML.getValue("manual_trans", true);
			params->curr_Scene   = XML.getValue("curr_Scene",   0);
			params->next_Scene   = XML.getValue("next_Scene",   0);
			params->trans_time   = XML.getValue("trans_time",   0.0);

			// BLEND MODES
			int b_mode = XML.getValue("blendModeCam", 0);
			if(b_mode == 1){
				params->blendModeCam = OF_BLENDMODE_ALPHA;
				midiOut.sendControlChange(1, 21, 127);
			}else if(b_mode == 2){
				params->blendModeCam = OF_BLENDMODE_ADD;
				midiOut.sendControlChange(1, 22, 127);
			}else{
				params->blendModeCam = OF_BLENDMODE_SCREEN;
				midiOut.sendControlChange(1, 23, 127);
			}

			b_mode = XML.getValue("blendModeVis", 0);
			if(b_mode == 1){
				params->blendModeVis = OF_BLENDMODE_ALPHA;
				midiOut.sendControlChange(1, 12, 127);
			}else if(b_mode == 2){
				params->blendModeVis = OF_BLENDMODE_ADD;
				midiOut.sendControlChange(1, 13, 127);
			}else{
				params->blendModeVis = OF_BLENDMODE_SCREEN;
				midiOut.sendControlChange(1, 14, 127);
			}

			// CAMERA PARAMETERS
			params->camPosX     = XML.getValue("camPosX"     , 0);
			params->camPosY     = XML.getValue("camPosY"     , 0);
			params->escalaVideo = XML.getValue("escalaVideo" , 0.0);
			params->mod_value   = XML.getValue("mod_value"   , 0.0);
			params->brt_value   = XML.getValue("brt_value"   , 0.0);
			params->sat_value   = XML.getValue("sat_value"   , 0.0);
			params->cont_value  = XML.getValue("cont_value"  , 0.0);
			params->video_hue   = XML.getValue("video_hue"   , 0);
			params->scaled_half_screen_inv = XML.getValue("scaled_half_screen_inv", 0.0);

			// X, Y POSITION OF CENTER OF SCREEN
			params->posMap.x = XML.getValue("posMapX", 0.0);
			params->posMap.y = XML.getValue("posMapY", 0.0);
			params->global_scale = XML.getValue("global_scale", 0.0);

			// FBO ALPHA VALUES
			params->opaFbo  = XML.getValue("opaFbo"  ,  0.0);
			params->opaGral = XML.getValue("opaGral" ,  0.0);
			params->opaTrans = XML.getValue("opaTrans", 0.0);
			XML.popTag();

			// LOCAL COLORS
			XML.pushTag("local_color");
			
			int num_tags = XML.getNumTags("color");
			printf("Num colors=%d\n", num_tags);

			for (int i = 0; i < num_tags; i++)
			{
				XML.pushTag("color", i);
				params->local_color[i].r = XML.getValue("R", 0);
				params->local_color[i].g = XML.getValue("G", 0);
				params->local_color[i].b = XML.getValue("B", 0);
				params->local_color[i].a = XML.getValue("A", 0);
				XML.popTag();
			}
			XML.popTag();

			// ACTIVE SCENES
			XML.pushTag("active_scenes");
			
			num_tags = XML.getNumTags("scene");
			printf("Num scenes=%d\n", num_tags);

			for (int i = 0; i < num_tags; i++)
			{
				XML.pushTag("scene", i);
				params->sc_flags[i] = XML.getValue("val", true);
				midiOut.sendControlChange(1, i, 127*params->sc_flags[i]);
				XML.popTag();
			}
			XML.popTag();

			// RANDOM SCENES
			XML.pushTag("random_scenes");
			
			num_tags = XML.getNumTags("scene");
			printf("Num scenes=%d\n", num_tags);

			for (int i = 0; i < num_tags; i++)
			{
				XML.pushTag("scene", i);
				params->activeScenes.push_back(XML.getValue("val", 0));
				XML.popTag();
			}
			XML.popTag();

			// MIDI CONTROLLER VALUES
			XML.pushTag("controller");

			num_tags = XML.getNumTags("value");
			printf("Num control=%d\n", num_tags);
			
			for (int i = 0; i < num_tags; i++)
			{
				XML.pushTag("value", i);
				controller[i] = XML.getValue("c_val", 0);
				XML.popTag();
			}
			XML.popTag();

			printf("Read settings from XML file\n");
		}
		else
		{
			printf("Using default settings\n");
			initControllerSetup();
		}
	}
	else
	{
		printf("No settings file...\n");
		initControllerSetup();
	}
}

//======================================//
