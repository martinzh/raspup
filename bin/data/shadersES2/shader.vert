varying vec2 texCoordVarying;

void main()
{

    texCoordVarying = gl_MultiTexCoord0.xy;
    // send the vertices to the fragment shader
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}